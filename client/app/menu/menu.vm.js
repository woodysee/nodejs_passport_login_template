(()=>{
    angular.module('App').controller('MenuVM', MenuVM);

    MenuVM.$inject = ["user"];

    function MenuVM(user) {
        const vm = this;
        vm.user = user;
    };
})();