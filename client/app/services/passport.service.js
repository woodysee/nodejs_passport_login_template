(()=>{
    angular.module("App").service("PassportSvc", PassportSvc);

    PassportSvc.$inject = ["$http"];

    function PassportSvc($http) {
        const svc = this;

        //Local user authentication
        svc.userAuth = () => {
            return $http.get("/user/auth");
        };

        svc.login = (user) => {
            return $http.post("/login", user);
        };

        //Local user creation
        svc.signup = (user) => {
            return $http.post("/");
        }
    }
})();