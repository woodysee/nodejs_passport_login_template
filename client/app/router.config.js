(()=>{
    angular.module("App").config(Router);
    Router.$inject = ["$stateProvider", "$urlRouterProvider"];

    function Router($stateProvider, $urlRouterProvider) {
        $stateProvider.state(
            "home", {
                url: "/home",
                views: {
                    menu: {
                        templateUrl: "app/menu/menu.html",
                        controller: "MenuVM as vm"
                    },
                    content: {
                        templateUrl: "app/home/home.html",
                        controller: "HomeVM as vm"
                    }
                },
                resolve: {
                    user: function(PassportSvc) {
                        return PassportSvc.userAuth().then((result)=>{
                            return result.data.user;
                        }).catch((err)=>{
                            return "";
                        });
                    }
                }
            }
        ).state(
            "login", {
                url: "/login",
                views: {
                    menu: {
                        templateUrl: "app/menu/menu.html",
                        controller: "MenuVM as vm"
                    },
                    content: {
                        templateUrl: "app/user/login.html",
                        controller: "LoginVM as vm"
                    }
                },
                resolve: {
                    user: function(PassportSvc) {
                            return PassportSvc.userAuth().then((result)=>{
                                return result.data.user;
                            }).catch((err)=>{
                                return "";
                            });
                        }
                }
            }
        ).state(
            "signup", {
                url: "/signup",
                views: {
                    menu: {
                        templateUrl: "app/menu/menu.html",
                        controller: "MenuVM as vm"
                    },
                    content: {
                        templateUrl: "app/user/signup.html",
                        controller: "SignupVM as vm"
                    }
                },
                resolve: {
                    user: function(PassportSvc) {
                            return PassportSvc.userAuth().then((result)=>{
                                return result.data.user;
                            }).catch((err)=>{
                                return "";
                            });
                        }
                }
            }
        );

        $urlRouterProvider.otherwise("/home");
    }
})();
