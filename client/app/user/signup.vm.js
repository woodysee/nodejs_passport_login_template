(()=>{
    angular.module("App").controller("SignupVM", SignupVM);

    SignupVM.$inject = ["PassportSvc", "$sanitize", "$state", "Flash"];

    function SignupVM(PassportSvc, $sanitize, $state, Flash) {
        const vm = this;
        vm.user = {
            username: "",
            password: "",
            confirm: ""
        };
        vm.msg = "";
        vm.signup = () => {
            PassportSvc.signup(
                $sanitize(vm.user.username),
                $sanitize(vm.user.password),
                $sanitize(vm.user.confirm)
            ).then((result)=>{
                $state.go("home");
                return true;
            }).catch((err)=>{
                vm.msg = "Invalid Username or Password!";
                vm.user.username = vm.user.password = "";
                return false;
            });
        };
    }
})();
