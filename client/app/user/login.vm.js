(()=>{
    angular.module("App").controller("LoginVM", LoginVM);

    LoginVM.$inject = ["PassportSvc", "$state"];

    function LoginVM(PassportSvc, $state) {
        const vm = this;
        vm.user = {
            username: "",
            password: ""
        };
        vm.msg = "";
        vm.login = () => {
            PassportSvc.login(vm.user).then((result)=>{
                $state.go("home");
                return true;
            }).catch((err)=>{
                vm.msg = "Invalid Username or Password!";
                vm.user.username = vm.user.password = "";
                return false;
            });
        };
    }
})();
