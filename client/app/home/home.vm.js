(function() {
    angular.module("App").controller("HomeVM", HomeVM);

    HomeVM.$inject = ["user"];

    function HomeVM(user) {
        const vm = this;
        vm.user = user;
    }
})();