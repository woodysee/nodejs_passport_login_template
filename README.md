# Node.js Passport Login Template #

User login & authentication with Node.js using `bcrypt`, `express`, `passport`, `sequelize` and `MySQL`

## Current features ##
* Local user authentication using Passport
* Social media user authentication using Passport (WIP)

## Planned features (Implemented halfway) ##
* Local user creation/deletion using Passport
* Social media user signup/creation/deletion using Passport

## Bower ##
Since Bower is deprecated and not supporting on some deployment services like Heroku, perform these additional steps to bind `bower` to `npm`.

* Install `bower` in the same fashion.
* In `/package.json`, edit `scripts`:

```
"scripts": {
    "postinstall": "bower install",
    "test": "echo \"Error: no test specified\" && exit 1"
  },
```
* Delete the existing `client/bower_components`.
* In future, just run `npm install` and it should update or reinstall both node and bower packages.

## References ##
- [Passport Docs](http://www.passportjs.org/docs)
- [Stackup](https://bitbucket.org/iss_startup/auth-passport-nodejs)
- [kenken77](https://bitbucket.org/kenken77/day-20-demo-weddinggram-auth)