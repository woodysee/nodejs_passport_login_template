module.exports = (auth, server, passport) => {

    // Local user authentication

    server.post(
        "/login",
        passport.authenticate("local"),
        (req, res) => {
            res.status(200).json({ user: req.user });
        }
    );

    server.get(
        "/user/auth",
        (req, res) => {
            if (req.user) {
                res.status(200).json({ user: req.user });
            } else {
                res.sendStatus(401);
            }
        }
    );

    server.get(
        "/logout",
        (req, res) => {
            req.logout();
            res.redirect("/");
        }
    );

    //Social media user authentication
    /* Facebook */
    
    server.get(
        "/auth/facebook",
        passport.authenticate("facebook")
    );
    
    server.get(
        "/auth/facebook/callback",
        passport.authenticate("facebook", { failureRedirect: "/login" }),
        (req, res) => {
            // Successful Facebook user authentication, redirect to home.
            res.redirect("/");
        }
    );

    /* Google */

    server.get(
        "/auth/google",
        passport.authenticate("google", {scope: ["profile"]})
    );

    server.get(
        "/auth/google/callback",
        passport.authenticate("google", { failureRedirect: "/login" }),
        (req, res) => {
            // Successful Google user authentication, redirect to home.
            res.redirect("/");
        }
    );

};
