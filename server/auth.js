/* 3 pieces need to be configured to use Passport for authentication:

    1. Authentication strategies
    2. Application middleware
    3. Sessions (optional)

*/

console.log("Importing authentication modules...");
/* Importing Passport modules... */
const LocalStrategy = require("passport-local").Strategy;
const FacebookStrategy = require("passport-facebook").Strategy;
const GoogleStrategy = require("passport-google-oauth20").Strategy;
const config = require("./config");

//This will depend on the localAuthentication, the user-defined callback
function authenticateUser(username, password) {
    const userDB = config.USER_DATABASE;

    //Keeps looping the userDB until the user is found
    for (let i = 0; i < userDB.length; i++) {
        let currentUser = userDB[i];
        // When the user is finally found...
        if (username == currentUser.username) {
            //...returns boolean based on whether the password input matches the record on userDB.
            return (password == currentUser.password);
        };
    };

    return false; // User authentication returns false by default as a security precaution.
};

module.exports = (app, passport) => {
     /* The local strategy's "verify" callback. In Passport, the purpose of a verify callback is to find the user that possesses a set of credentials. It then invokes the verify callback with those credentials as arguments, in this case `username` & `password`. `done` is also passed as an argument to allow Passport to be supplied the user that is being authenticated. */
    const localAuthentication = (username, password, done) => {
        //console.log("In authenticate function(): username=%s, password=%s", username, password);
        const valid = authenticateUser(username, password);
        if (valid) {
            //In Passport, if the credentials are valid, the verify callback invokes `done` to supply Passport with the user that authenticated.
            return done(null, username);
        } else {
            //In Passport, if the credentials are not valid (e.g. if the password is incorrect), `done` should be invoked with `false` instead of a user to indicate an authentication failure. An optional additional info message can be supplied to indicate the reason for the failure. This is useful for displaying a flash message prompting the user to try again.
            return done(null, false, { message: 'Incorrect user credentials were entered.' });
        };
    };
    /* Local Strategy */
    passport.use(new LocalStrategy(
        {
            // redefine the field names the strategy (passport-local) expects
            usernameField: 'username',
            passwordField: 'password'
        },
        localAuthentication
    ));
    /* Social Media Strategies */
    // Facebook Strategy by Jared Hanson: https://github.com/jaredhanson/passport-facebook
    /* Passport strategy for authenticating with Facebook using the OAuth 2.0 API. */
    passport.use(new FacebookStrategy(
        {
            clientID: config.FACEBOOK_APP_ID,
            clientSecret: config.FACEBOOK_APP_SECRET,
            callbackURL: `http://${config.HOST}:${config.PORT}/auth/facebook/callback`
        },
        function(accessToken, refreshToken, profile, cb) {
            User.findOrCreate({ facebookId: profile.id }, function (err, user) {
                return cb(err, user);
            });
        }
    ));
    //Google Strategy by Jared Hanson: https://github.com/jaredhanson/passport-google-oauth2
    /* Passport strategy for authenticating with Google using the OAuth 2.0 API. */

    passport.use(new GoogleStrategy(
        {
            clientID: config.GOOGLE_CLIENT_ID,
            clientSecret: config.GOOGLE_CLIENT_SECRET,
            callbackURL: `http://${config.HOST}:${config.PORT}/auth/google/callback`
        },
        function(accessToken, refreshToken, profile, cb) {
            User.findOrCreate({ googleId: profile.id }, function (err, user) {
                return cb(err, user);
            });
        }
    ));

    /* Passport sessions */
    /* The credentials used to authenticate a user will only be transmitted during the login request. If authentication succeeds, a session will be established and maintained via a cookie set in the client. Each subsequent request will not contain credentials, but rather the unique cookie that identifies the session. In order to support login sessions, Passport will serialize and deserialize user instances to and from the session. */

    passport.serializeUser((username, done)=>{
        //console.log('passport.serializeUser: ' + username);
        done(null, username);
    });
    passport.deserializeUser((id, done)=>{
        //console.log('passport.deserializeUser: ' + id);
        const user = id;
        done(null, user);
    });
    const isAuthenticated = (req, res, next) => {
        //console.log("isAuthenticated(): ", req.user);
        if (req.isAuthenticated()) {
            next();
        }
        else {
            res.sendStatus(401);
        };
    };
    return { isAuthenticated: isAuthenticated };
};

//Documentation references: http://www.passportjs.org/docs