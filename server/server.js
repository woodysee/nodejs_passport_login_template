/* Importing node.js packages... */
console.log("Importing node.js packages...");
const express = require("express");
const bodyParser = require("body-parser");
const Sequelize = require("sequelize");
const dotenv = require("dotenv");
const path = require("path");
const session = require("express-session");
const passport = require("passport");


/* Declaring environment variables... */
console.log("Declaring environment variables...");
dotenv.config(); //Environment config should be declared as early on as possible
const config = require("./config"); //Pointing it to just the config folder defaults pointing to index.js
const PORT = process.env.PORT || config.PORT || 3000;

/* Initialising Express server... */
console.log("Initialising Express server...");
const server = express();

/* Using body-parser... */
server.use(bodyParser.urlencoded({ extended: false }));
server.use(bodyParser.json());

server.use(
    session({
        secret: config.SECRET,
        resave: true,
        saveUninitialized: true
    })
);
/* Using PassportJS... */
server.use(passport.initialize());
server.use(passport.session());

/* Configuring routes... */
server.use(express.static(__dirname + "/../client/"));

/* Initialising Sequelize */
console.log("Initialising Sequelize connection to MySQL database...");
const connection = new Sequelize("passport_template", MYSQL_USER, MYSQL_PASS, {
    host: "localhost",
    port: 3306,
    logging: console.log,
    dialect: "mysql",
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    }
});
const Op = Sequelize.Op;

const User = require("./models/user")(connection, Sequelize);

/* Declaring routes for user authentication by Passport... */
const auth = require("./auth")(server, passport);
require("./auth.routes")(auth, server, passport);

/* Mandatory express.js server listener */
server.listen(PORT, () => {
    console.log(`...express.js server is now running locally on your port ${PORT}:`);
});