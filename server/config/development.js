'use strict';

module.exports = {
    PORT: process.env.PORT,
    SECRET: process.env.SECRET,
    FACEBOOK_APP_ID: process.env.FACEBOOK_APP_ID,
    FACEBOOK_APP_SECRET: process.env.FACEBOOK_APP_SECRET,
    GOOGLE_CLIENT_ID: process.env.GOOGLE_CLIENT_ID,
    GOOGLE_CLIENT_SECRET: process.env.GOOGLE_CLIENT_SECRET,
    USER_DATABASE: [
        {
            username: 'adam',
            password: 'mada'
        },
        {
            username: 'betty',
            password: 'ytteb'
        },
        {
            username: 'charles',
            password: 'selrahc'
        },
        {
            username: 'denise',
            password: 'esined'
        },
        {
            username: 'eric',
            password: 'cire'
        }
    ],
    version: '1.0',
};