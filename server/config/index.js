"use strict";

const ENV = process.env.NODE_ENV || "development";
console.log(`Using config/${ENV}.js for this environment...`);

module.exports = require(__dirname + "/" + ENV) || {};